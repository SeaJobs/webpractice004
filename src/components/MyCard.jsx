import React, { Component } from 'react'
import {Card,Button} from 'react-bootstrap';

export default class MyCard extends Component {

    render() {
        return (
            <div>
                <Card style={{ width: '18rem' }}>
                <Card.Img variant="top" src={this.props.image}/>
                <Card.Body>
                    <Card.Title>{this.props.title}</Card.Title>
                    <Card.Text>{this.props.description}</Card.Text>
                    <Button variant="danger">delete</Button>
                </Card.Body>
            </Card>
                
            </div>
        )
    }
}
