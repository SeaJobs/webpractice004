import React, { Component } from 'react'
import {Container,Row,Col} from 'react-bootstrap'
import MyNav from './components/MyNav'
import MyCard from './components/MyCard'

export default class App extends Component {

  constructor(props){
    super(props)
    this.state = {
     data:[
          {  id: 1,
             image: 'images/react.png',
             title: 'ReactJS',
             description: "ReactJS is JS library using to build Reusable UI Components",
           },
           { 
             id: 2,
             image: 'images/spring.png',
             title: 'Spring',
             description: "Spring is the Java Frameword using to build FrontEnd API ",
           },
         { id: 3,
           image: 'images/korea.jpg',
           title: 'Korea',
           description: "Korea Languauge is the best to learn for Working and study Master degree in Korea",
        }
     ]
    }
}

onDeleteClick=(index)=>{
    const itemCard= Object.assign([],this.state.data); 
   itemCard.splice( index,1); 
  this.setState({data:itemCard})
  }
  render() {
    return (
      <Container style = {{paddingTop:70}}>
         <h1>Practice004 </h1> <br></br>
      <MyNav/><br></br>

      <Row>
        {this.state.data.map((data,index) =>{
                 return(
                 <Col key={data.id} > 
                  <MyCard image={data.image} title={ data.title}  
                  description={data.description} 
                  onDeleteClick={ ()=>this.onDeleteClick(index)}/>  
                  </Col>)    
                  }   
              )        
         }    
      </Row>
    </Container>
    )
  }
}
